var express = require('express');
var router = express.Router();
const data = require('../resources/ncco.json')
const datareply = require('../resources/nccoreply.json')

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/ncco', function(req, res, next) {
  data[1].eventUrl = [process.env.SERVER + '/ivr']
  res.json(data);
});

router.post('/ivr', function(req, res, next) {
console.log(req.body);
if (req.body.timed_out) res.json(data);
else
  {
    datareply[0].text = req.body.dtmf;
    datareply[2].eventUrl = [process.env.SERVER + '/ivr'];
    res.json(datareply);
}
});

module.exports = router;
